export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  color: string;
  /**
   * List of tracks
   */
  tracks?: Track[] 
}

interface Track {}
type Tracks = Track[];
