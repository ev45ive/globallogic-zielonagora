import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlaylistsViewComponent } from "./playlists-view/playlists-view.component";
import { PlaylistDetailsComponent } from "./playlist-details/playlist-details.component";
import { PlaylistContainerComponent } from "./playlist-container/playlist-container.component";
import { PlaylistsContainerComponent } from "./playlists-container/playlists-container.component";

const routes: Routes = [
  {
    path: "playlist",
    component: PlaylistsViewComponent,
    children: [
      {
        path: ":id",
        component: PlaylistsContainerComponent,
        outlet: "list"
      },
      {
        path: "",
        component: PlaylistsContainerComponent,
        outlet: "list"
      },
      {
        path: ":id",
        component: PlaylistContainerComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule {}
