import { Component, OnInit } from "@angular/core";
import { PlaylistService } from "../playlist.service";
import { Playlist } from "src/app/model/Playlist";
import { ActivatedRoute } from "../../../../node_modules/@angular/router";
import { Observable } from "rxjs";
import { map, switchMap } from "rxjs/operators";

@Component({
  selector: "app-playlist-container",
  templateUrl: "./playlist-container.component.html",
  styleUrls: ["./playlist-container.component.css"]
})
export class PlaylistContainerComponent implements OnInit {
 
  selected = this.route.paramMap.pipe(
    map(paramMap => parseInt(paramMap.get("id"))),
    switchMap(id => this.service.getPlaylist(id))
  );

  save(playlist: Playlist) {
    this.service.save(playlist);
  }

  constructor(private route: ActivatedRoute, private service: PlaylistService) {
    // const id = parseInt(route.snapshot.paramMap.get("id"),10);
  }

  ngOnInit() {}
}
