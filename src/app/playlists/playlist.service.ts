import { Injectable } from "@angular/core";
import { Playlist } from "../model/Playlist";
import { BehaviorSubject, of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class PlaylistService {
  playlists = new BehaviorSubject<Playlist[]>([
    {
      id: 123,
      name: "Angular Hits",
      favourite: false,
      color: "#ff0000"
    },
    {
      id: 234,
      name: "Angular TOP20",
      favourite: true,
      color: "#00ff00"
    },
    {
      id: 345,
      name: "The Best of Angular",
      favourite: false,
      color: "#0000ff"
    }
  ]);
  selected = new BehaviorSubject<Playlist>(null);

  getPlaylists() {
    return this.playlists.asObservable();
  }

  getPlaylist(id: number) {
    return of(this.playlists.getValue().find(p => p.id == id))
  }

  getSelected() {
    return this.selected.asObservable();
  }

  select(playlist: Playlist) {
    this.selected.next(playlist);
  }

  save(playlist: Playlist) {
    const playlists = this.playlists.getValue();

    const index = playlists.findIndex(p => p.id == playlist.id);
    if (index !== -1) {
      playlists.splice(index, 1, playlist);
    }
    this.playlists.next(playlists);
    this.selected.next(playlist);
  }

  constructor() {}
}
