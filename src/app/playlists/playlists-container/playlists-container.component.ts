import { Component, OnInit } from "@angular/core";
import { map, switchMap, tap } from "rxjs/operators";
import { Playlist } from "src/app/model/Playlist";
import { ActivatedRoute, Router } from "../../../../node_modules/@angular/router";
import { PlaylistService } from "../playlist.service";

@Component({
  selector: "app-playlists-container",
  templateUrl: "./playlists-container.component.html",
  styleUrls: ["./playlists-container.component.css"]
})
export class PlaylistsContainerComponent implements OnInit {
  playlists = this.service.getPlaylists()

  selected = this.route.paramMap.pipe(
    tap(console.log),
    map(paramMap => parseInt(paramMap.get("id"))),
    switchMap(id => this.service.getPlaylist(id))
  );

  select(playlist: Playlist) {
    this.router.navigate(["/playlist", playlist.id], {
      // relativeTo: this.route,
    });
  }

  save(playlist: Playlist) {
    this.service.save(playlist);
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: PlaylistService
  ) {}
  ngOnInit() {}
}
