import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
  Output
} from "@angular/core";
import { NgForOfContext } from "@angular/common";
NgForOfContext;

interface Item {
  id: number;
  name: string;
  color: string;
}

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated
  // inputs:['playlists:items']
})
export class ItemsListComponent<T extends Item> implements OnInit {
  hover: T;

  @Input()
  playlists: T[] = [];

  @Output()
  selectedChange = new EventEmitter<T>();

  @Input()
  selected: T = null;

  select(playlist: T) {
    this.selectedChange.emit(playlist);
  }

  trackFn(index, item) {
    // console.log(index,item)
    return item.id;
  }

  constructor() {}

  ngOnInit() {}
}
