import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Playlist } from "../../model/Playlist";


@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.css"]
})
export class PlaylistDetailsComponent implements OnInit {
  @Input()
  playlist: Playlist;

  @Output()
  playlistChange = new EventEmitter<Playlist>();

  mode: "show" | "edit" = "show";

  constructor() {}

  ngOnInit() {}

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  save(form) {
    const playlist = {
      ...this.playlist,
      ...(form.value as Partial<Playlist>)
    };
    this.playlistChange.emit(playlist);
    this.mode = "show";
  }
}
