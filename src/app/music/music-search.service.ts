import { Injectable, Inject, InjectionToken } from "@angular/core";
import { Album } from "../model/album";
import { HttpClient } from "@angular/common/http";
import { AlbumsResponse } from "../model/album";

export const SEARCH_API_URL = new InjectionToken<string>(
  "Search API URL TOKEN"
);
import { map, startWith, switchMap } from "rxjs/operators";
import { Observable, of, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class MusicSearchService {
  albums$ = new BehaviorSubject<Album[]>([]);
  query$ = new BehaviorSubject<string>("batman");

  constructor(
    @Inject(SEARCH_API_URL) private api_url: string,
    private http: HttpClient
  ) {
    
    this.query$
      .pipe(
        map(query => ({
          q: query,
          type: "album"
        })),
        switchMap(params => this.http.get<AlbumsResponse>(this.api_url, { params })),
        // o => o,
        map(resp => resp.albums.items)
      )
      .subscribe(albums => {
        this.albums$.next(albums);
      });
  }

  //Commands:
  search(query: string) {
    this.query$.next(query);
  }

  // Queries:
  getAlbums() {
    return this.albums$.asObservable();
  }
}
