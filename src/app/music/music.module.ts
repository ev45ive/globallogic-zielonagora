import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MusicSearchComponent } from "./music-search/music-search.component";
import { SearchFormComponent } from "./search-form/search-form.component";
import { AlbumsGridComponent } from "./albums-grid/albums-grid.component";
import { AlbumCardComponent } from "./album-card/album-card.component";
import { environment } from "../../environments/environment";
import { SEARCH_API_URL } from "./music-search.service";
// import { MusicSearchService } from "./music-search.service";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { MusicProviderDirective } from './music-provider.directive';

@NgModule({
  imports: [CommonModule, HttpClientModule, ReactiveFormsModule],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumCardComponent,
    MusicProviderDirective
  ],
  exports: [MusicSearchComponent, MusicProviderDirective],
  providers: [
    {
      provide: SEARCH_API_URL,
      useValue: environment.api_url
    }
    // {
    //   provide: MusicSearchService,
    //   useFactory: (url) => {
    //     return new MusicSearchService(url)
    //   },
    //   deps:[SEARCH_API_URL]
    // },
    // {
    //   provide: AbstractMusicSearchService,
    //   useClass: SpotifyMusicSearchService,
    //   // deps:[SEARCH_API_URL]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass:MusicSearchService
    // },
    // MusicSearchService
  ]
})
export class MusicModule {}
