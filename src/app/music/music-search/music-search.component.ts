import { Component, OnInit, Inject } from "@angular/core";
import { Album } from "../../model/album";
import { MusicSearchService } from "../music-search.service";
import { Subscription, Subject } from "rxjs";
import {
  takeUntil,
  tap,
  multicast,
  publish,
  share
} from "rxjs/operators";

@Component({
  selector: "app-music-search",
  templateUrl: "./music-search.component.html",
  styleUrls: ["./music-search.component.css"]
})
export class MusicSearchComponent implements OnInit {
  albums$ = this.service
    .getAlbums()
    .pipe(tap(albums => (this.albums = albums)));

  query$ = this.service.query$

  albums: Album[];

  constructor(private service: MusicSearchService) {}

  search(query: string) {
    this.service.search(query);
  }

  message = "";

  ngOnInit() {}
}
