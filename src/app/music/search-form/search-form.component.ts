import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  FormGroup,
  AbstractControl,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
  ValidatorFn,
  AsyncValidatorFn,
  ValidationErrors
} from "@angular/forms";
import {
  filter,
  distinctUntilChanged,
  debounceTime,
  combineLatest,
  withLatestFrom
} from "rxjs/operators";
import { Observable, empty, Observer } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.css"]
})
export class SearchFormComponent implements OnInit {
  queryForm: FormGroup;

  @Output()
  queryChange = new EventEmitter();

  @Input()
  set query(value) {
    this.queryForm.get("query").setValue(value, {
      emitEvent: false
    });
  }

  constructor(private bob: FormBuilder) {
    const censor = (badword: string): ValidatorFn => (
      control: AbstractControl
    ): ValidationErrors | null => {
      const hasError = (control.value as string).includes(badword);

      return hasError
        ? {
            censor: {
              badword: badword
            }
          }
        : null;
    };

    const asyncCensor = (badword: string): AsyncValidatorFn =>
      //
      (control: AbstractControl): Observable<ValidationErrors | null> => {
        // return this.http.get(..).map(res => errors)
        //
        return Observable.create(
          (observer: Observer<ValidationErrors | null>) => {
            //
            const hasError = (control.value as string).includes(badword);

            const handler = setTimeout(() => {
              observer.next(
                hasError
                  ? {
                      censor: {
                        badword: badword
                      }
                    }
                  : null
              );
              observer.complete();
            }, 2000);

            // onUnsubscribe
            return () => {
              clearTimeout(handler);
            };
          }
        );
      };

    this.queryForm = this.bob.group({
      query: this.bob.control(
        "",
        [Validators.required, Validators.minLength(3) /* , censor("batman") */],
        [asyncCensor("batman")]
      )
    });

    console.log(this.queryForm);

    const value$ = this.queryForm.get("query").valueChanges.pipe(
      debounceTime(400),
      filter((query: string) => query.length >= 3),
      distinctUntilChanged()
    );
    const status$ = this.queryForm.get("query").statusChanges;

    const valid$ = status$.pipe(filter(status => status === "VALID"));

    const search$ = valid$.pipe(
      // combineLatest(value$, (_, value) => value)
      withLatestFrom(value$, (_, value) => value)
    );

    search$.subscribe(query => {
      this.search(query);
    });
  }

  ngOnInit() {}

  search(query: string) {
    // console.log(query);
    this.queryChange.emit(query);
  }
}
