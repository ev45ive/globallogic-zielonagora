import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { SharedModule } from './shared/shared.module';
import { MusicModule } from './music/music.module';
import { SecurityModule } from './security/security.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    SharedModule,
    BrowserModule,
    PlaylistsModule,
    AppRoutingModule,
    MusicModule,
    SecurityModule.forRoot({
      // auth_url:''
    })
  ],
  // entryComponents:[AppComponent],
  providers: [],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { 

  // constructor(private app:ApplicationRef){}
  // ngDoBootstrap(){
  //   this.app.bootstrap(AppComponent)
  // }
}
