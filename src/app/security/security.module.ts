import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { environment } from "../../environments/environment";
import { AuthConfig, SecurityService } from "./security.service";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptorService } from "./auth-interceptor.service";

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [
    {
      provide: AuthConfig,
      useValue: environment.auth_config
    }
  ]
})
export class SecurityModule {
  
  constructor(private security: SecurityService) {
    this.security.getToken();
  }

  static forRoot(params: {}): ModuleWithProviders {
    return {
      ngModule: SecurityModule,
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptorService,
          multi: true
        }
      ]
    };
  }

  static forChild(params: {}): ModuleWithProviders {
    return {
      ngModule: SecurityModule,
      providers: []
    };
  }
}
