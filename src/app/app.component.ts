import { Component } from "@angular/core";

@Component({
  selector: "app-root, li[app-root], .placki:not(.czekoladowe)",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "Music App";
}
