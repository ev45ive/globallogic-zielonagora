import { Component, OnInit, Input, EventEmitter } from "@angular/core";;

@Component({
  selector: "app-tab",
  templateUrl: "./tab.component.html",
  styleUrls: ["./tab.component.css"]
})
export class TabComponent implements OnInit {
  @Input()
  title = "";

  open = false;

  openChange = new EventEmitter()

  toggle() {
    this.openChange.emit()
    // this.open = !this.open;
  }

  constructor(
    // private tabs: TabsComponent
  ) {
    // console.log(tabs)
  }

  ngOnInit() {}
}
