import {
  Component,
  OnInit,
  ViewChild,
  ContentChild,
  ContentChildren,
  QueryList
} from "@angular/core";
import { TabsNavComponent } from "../tabs-nav/tabs-nav.component";
import { TabComponent } from "../tab/tab.component";

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.component.html",
  styleUrls: ["./tabs.component.css"]
})
export class TabsComponent implements OnInit {
  // @ViewChild('navs')
  @ViewChild(TabsNavComponent)
  navs: TabsNavComponent;

  @ContentChildren(TabComponent)
  tabs: QueryList<TabComponent>;

  activeTab = null

  constructor() {}

  ngOnInit() {}

  ngAfterContentInit() {
    const links = [];
    // console.log(this.tabs);
    this.tabs.forEach(tab => {
      links.push(tab.title);

      tab.openChange.subscribe(()=>{
        this.activeTab = tab;
      })
    });
    this.navs.links = links
  }

  ngAfterViewInit() {
    console.log(this.navs);
  }
}
