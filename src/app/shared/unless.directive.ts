import {
  Directive,
  TemplateRef,
  ViewContainerRef,
  Input,
  ViewRef,
  ComponentFactoryResolver
} from "@angular/core";
import { PlaylistsViewComponent } from "../playlists/playlists-view/playlists-view.component";

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {
  cache: ViewRef;

  @Input()
  set appUnless(hide) {
    if (hide) {
      // this.vcr.clear();
      this.cache = this.vcr.detach();
    } else {
      if (this.cache) {
        this.vcr.insert(this.cache);
      } else {
        // this.vcr.createEmbeddedView(this.tpl, {
        //   $implicit: "placki malinowe!",
        //   message: "Lubi placki"
        // });
        const f = this.resolver.resolveComponentFactory(PlaylistsViewComponent)

        const cref = this.vcr.createComponent(f,0)
        // cref.instance
      }
    }
  }

  constructor(
    private resolver: ComponentFactoryResolver,
    private tpl: TemplateRef<any>, 
    private vcr: ViewContainerRef) {}
}
