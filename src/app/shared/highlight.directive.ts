import {
  Directive,
  ElementRef,
  Input,
  OnInit,
  DoCheck,
  OnChanges,
  SimpleChanges,
  Renderer2,
  HostBinding,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[appHighlight]"
  // host:{
  //   '[style.color]':'appHighlight',
  //   '(mouseenter)':'activate($event)'
  // }
})
export class HighlightDirective implements OnInit, DoCheck, OnChanges {

  @Input()
  appHighlight;

  @HostBinding("style.color")
  get currentColor(){
    return this.hover? this.appHighlight : 'unset'
  }

  @HostBinding("class.hover")
  hover = false;

  constructor() {}

  @HostListener("mouseenter")
  activate() {
    this.hover = true;
  }

  @HostListener("mouseleave")
  deactivate() {
    this.hover = false;
  }

  ngOnInit() {
    // console.log("Hello", this.appHighlight);
  }

  ngDoCheck() {
    // console.log('ngDoCheck')
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log('ngOnChanges',changes)
  }
}