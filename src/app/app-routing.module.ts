import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MusicSearchComponent } from "./music/music-search/music-search.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlist",
    pathMatch: "full"
  },
  {
    path: "search",
    component: MusicSearchComponent
  },
  {
    path: "**",
    redirectTo: "playlist",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // enableTracing: true,
      // useHash: true,
      paramsInheritanceStrategy: "always"
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
